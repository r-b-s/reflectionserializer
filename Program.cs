﻿using Newtonsoft.Json;
using reflectionSerializer;
using System.Diagnostics;


List<string> arr = new List<string>();
var f = new F();
var stopwatch = new Stopwatch();
const int cnt = 100000;


stopwatch.Start();
for (int i = 0; i <cnt ; i++)
    arr.Add( Serializer.Serialize(f.Get()));
stopwatch.Stop();
Console.WriteLine($"Сериализация {cnt} объектов: {stopwatch.ElapsedMilliseconds} ms");
Console.WriteLine("Нажмите любую клавишу");
Console.ReadKey();

stopwatch.Reset();
stopwatch.Start();
foreach (string a in arr)
    Console.Write(a);
stopwatch.Stop();
Console.WriteLine($"результат выведен за: {stopwatch.ElapsedMilliseconds} ms");


stopwatch.Reset();
List<object> o = new List<object>();
stopwatch.Start();
foreach (string a in arr)
   o.Add( Serializer.Deserialize(a));
stopwatch.Stop();
Console.WriteLine($"Десериализация из строки: {stopwatch.ElapsedMilliseconds} ms");



List<string> json = new List<string>();
stopwatch.Start();
for (int i = 0; i < cnt; i++)
    json.Add( JsonConvert.SerializeObject(f.Get()));
stopwatch.Stop();
Console.WriteLine($"Сериализация в JSON: {stopwatch.ElapsedMilliseconds} ms");
stopwatch.Reset();

o = new List<object>();
stopwatch.Start();
foreach(string j in json)
    o.Add(JsonConvert.DeserializeObject<F>(j));
stopwatch.Stop();
Console.WriteLine($"Десериализация из JSON: {stopwatch.ElapsedMilliseconds} ms");
stopwatch.Reset();

Console.ReadKey();



public class F
{
    public int i1, i2, i3, i4, i5;
    public F Get() => new F() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };
}