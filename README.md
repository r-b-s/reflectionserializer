������������� �����: class F { int i1, i2, i3, i4, i5;}
���������� �������: 100000 ��������
��� ���������:
����� �� ������������ = 208 ��
����� �� �������������� = 497 ��
 (NewtonsoftJson):
����� �� ������������ = 950 ��
����� �� �������������� = 265 ��


��� ������������-��������������: 



    public static  class Serializer
    {
        public static string Serialize(object obj)
        {
            var r = new StringBuilder();

            r.Append(obj.GetType().FullName);
            r.Append("#:#");

            foreach (var t in obj.GetType().GetFields())
            {                
                r.Append(t.Name);
                r.Append("='");
                r.Append(obj.GetType().GetField(t.Name).GetValue(obj));
                r.Append("' ");
            }
            return r.ToString();
        }

        public static object Deserialize(string obj)
        {
            var hdrBody = obj.Split("#:#");
            var objType = Type.GetType(hdrBody[0]);
            var r = Activator.CreateInstance(objType);

            var fields = hdrBody[1].Split("' ");
            foreach (var f in fields)
            {
                if (f == "") break;
                var fieldName = f.Split("='")[0];
                var value = f.Split("='")[1];
                var field =    objType.GetField(fieldName);  
                field.SetValue(r, Convert.ChangeType(value, field.FieldType));
                               
            }


            return r;
        }

    }